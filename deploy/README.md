#### Correr proyecto en localhost

- Paso 1: Desde la terminal de linux darte permisos para ejecutar el archivo init.sh con el siguiente comando 'chmod +x init.sh'
- Paso 2: Desde la ruta del archivo escriba './init.sh' en la terminal. 

>Esto ejecuta instrucciones de linux para automatizar el build y run de las imagenes: python-django y reactjs.

IMAGES & PORTS:
reactjs: 3000
python-django: 8000
