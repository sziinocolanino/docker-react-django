##### DevOps Project by: sziinocolanino@gmail.com

                                        <ReactJs, Django and Docker project>

### ReactJs

 >Con nuestro frontend en ReactJs nos comunicamos con Django a travez de Axios. Treamos la data por la ruta :8000/wel. De esta forma mostramos en pantalla la request que carguemos en Django a travez de los inputs: usuario y password.

### Django

Usamos Django para realizar una *REST API* facil y simple, donde podemos llenar dos inputs: usuario y password, esto se almacena en la *DBSQLite*, para luego consumirlo con el frontend-reactjs.

Creamos un entorno virutal para mantener las versiones que necesitemos sin colisionar con los demas. Pero vamos a ignorarlo en el .dockerignore. para luego instalar las librerias necesarias dentro del contenedor a la hora de buildear y correr el proyecto.

-Librarie & version installed in docker container:
 - djangorestframework 3.12.4
 - django-cors-headers 3.7.0
 - asgiref 3.3.1
 - Django 3.1.7
 - pytz 2021.1
 - sqlparse 0.4.1

# ¿Como arrancar el proyecto en localhost?

##### Para correr este proyecto en tu local necesitamos instalado localmente:
- Docker
- Docker-compose

https://docs.docker.com/engine/install/ubuntu/

>   En el folder 'deploy' por un lado tenemos el docker-compose.yml que esta preparado para buildear y/o correr las dos imagenes que generamos con el frontend-reactjs y el backend-django.
 
### /deploy

Cuando nos posicionamos en la carpeta /deploy ejecutamos en terminal el siguiente comando 'chmod +x init.sh', 
de esta forma le das a tu ordenador los permisos necesarios para ejecutar los scripts.

 El archivo lo ejecutamos escribiendo './init.sh' en la terminal y automaticamente va a buildear el Frontend y el Backend, una vez finalizado va a correr las dos imagenes.

 > Al final del init.sh scriptea 'docker-compose up' y corre las dos imagenes.

###  PORTS
>Los puertos expuestos seran:
    - python-django: 8000
    - reactjs: 3000


## ¿Como arrancar las imagenes individualmente?

#### In /frontend-reactjs
>En frontend-reactjs buildear Dockerfile con: docker build --tag frontend-reactjs
Y correr la imagen buildeada con el siguiente comando: docker run --publish 3000:3000 frontend-reactjs
de esta forma arrancamos la imagen llamada "frontend-reactjs" en el puerto 3000:3000 publicamente

#### In /backend-django/project
>Lo mismo con el Dockerfile del back: docker build --tag  backend-django
buildea la imagen e instala los requirements.txt, que son las librerias necesarias para el run.
Correr la imagen de la siguiente forma: docker run --publish 8000:8000 backend-django

---

# ¿Como deployarlo en una EC2?

In progress...
